from http.server import HTTPServer, BaseHTTPRequestHandler
import traceback
import download
import upload

class Serv(BaseHTTPRequestHandler):
    check = True
    def do_GET(self):
        # when the user connect to local host
        if self.path == "/":
            self.path = "/main.html"
        try:
            # open user file
            file_to_open = open(self.path[1:], "rb").read()
            self.send_response(200)
        except:
            traceback.print_exc()
            file_to_open = "no"
            self.send_response(404)
        try:
            self.end_headers()
            # send file to the user
            self.wfile.write(file_to_open)
        except:
            traceback.print_exc()

    def do_POST(self):
        global check
        try:
            self.handlePostRequest()
            # check if the user request file
            if check:
                file_to_open = open(self.path[1:], "rb").read() # open user file
            self.send_response(200)
        except:
            traceback.print_exc()
            file_to_open = "no"
            self.send_response(404)
        try:
            if check:
                self.end_headers()
                # send the file to the user
                self.wfile.write(file_to_open)
        except:
            traceback.print_exc()

    def handlePostRequest(self):
        global check
        check = True
        print(self.path)
        # when the user connect to local host
        if self.path == "/":
            self.path = "/main.html"
        # if the user want to see which files he have
        if self.path == "/loadDocs":
            upload.readFiles()
            self.path = "/docsFile.txt"
        # upload user file
        elif self.path.count("/upload?") > 0:
            upload.getFile()
            check = False
        # send file to the user
        elif self.path.count("/download") > 0:
            self.path = download.changeFilePath(self.path)
            check = True

httpd = HTTPServer(('localhost', 80), Serv)
httpd.serve_forever()

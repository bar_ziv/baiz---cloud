function downloadFile(id, name) {
    var _OBJECT_URL;
    var request = new XMLHttpRequest();
    request.addEventListener('readystatechange', function(e) {
    if(request.readyState == 2 && request.status == 200) {
        document.querySelector('#start-download').style.display = 'block';
        document.getElementById(id).style.display = "none";
    }
    else if(request.readyState == 3) {
        document.querySelector('#download-progress-container').style.display = 'block';
        document.querySelector('#start-download').style.display = 'none';
    }
    else if(request.readyState == 4) {
        _OBJECT_URL = URL.createObjectURL(request.response);

        document.querySelector('#save-file').setAttribute('href', _OBJECT_URL);
        document.querySelector('#save-file').setAttribute('download', name);
    		
        document.querySelector('#save-file').style.display = 'block';
        document.querySelector('#download-progress-container').style.display = 'none';

        setTimeout(function() {
            window.URL.revokeObjectURL(_OBJECT_URL);
                  
            document.querySelector(id).style.display = 'block';
            document.querySelector('#save-file').style.display = 'none';
            }, 60*1000);
        }
    });
    request.addEventListener('progress', function(e) {
        var percent_complete = (e.loaded / e.total)*100;
        document.querySelector('#download-progress').style.width = percent_complete + '%';
    });
    request.responseType = 'blob';
    var downloadFileName = "download?" + name;
    request.open('post', downloadFileName); 
    request.send(); 
}

function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("files").innerHTML = this.responseText;
    }
  };
  xhttp.open("POST", "/loadDocs", true);
  xhttp.send();
}

loadDoc();
import os
import flask
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = '/files'
app = flask.Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

class upload:
    # still doesn't documented
    def getFile():
        if flask.request.method == 'POST':
        # check if the post request has the file part
            if 'file' not in flask.request.files:
                flask.flash('No file part')
                return flask.redirect(flask.request.url)
            file = flask.request.files['file']
            # if user does not select file, browser also
            # submit a empty part without filename
            if file.filename == '':
                flask.flash('No selected file')
                return flask.redirect(flask.request.url)
            if file:
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                return flask.redirect(flask.url_for('uploaded_file', filename=filename))
        return

def readFiles():
    path = "files"
    fileOb = open("docsFile.txt", "r+")
    fileOb.truncate(0) # need '0' when using r+

    var = os.listdir(path)
    for fileInDir in var:
        name = fileInDir
        name = name.replace(" ", "%20")
        id = name[:-4]
        href = "downloadFile('" + id + "', '" + name + "')"
        fileOb.write('<a onclick="' + href + '" class="btn btn-outline-info btn-lg btn-block" id="' + id + '"><button>' + fileInDir + '</button></a>' + '\n' + '<br>')

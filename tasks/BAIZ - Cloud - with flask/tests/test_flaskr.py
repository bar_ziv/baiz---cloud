import requests
s = requests.Session()

def test_get_request_from_allowed_url(url):
    r = s.get(url = url)
    assert not ("404" in r.content.decode())

def test_get_request_from_not_allowed_url(url):
    r = s.get(url = url)
    assert ("404" in r.content.decode()) or ("index.html" in r.url)

def login_request(url, username, password):
    """first login"""
    data = {"username": username,
            "password": password }
    r = s.post(url = url, data=data)
    assert ("login.html" in r.url)

    """second login, we save the cookies for the sec login"""
    secPass = input("enter the email password: ")
    data = {"pass": secPass }
    r = s.post(url = "http://localhost/login.html", data=data)
    assert ("main.html" in r.url)

def signup_request(url, username, password, email):
    data = {"email": email,
            "username": username,
            "password": password }
    r = s.post(url = url, data=data)
    assert ("index.html" in r.url)

def add_password_for_user(url, website, username, password):
    data = {"website": website,
            "username": username,
            "password": password }
    r = s.post(url = url, data=data)
    assert ("passwords.html" in r.url)

    """second login, we save the cookies for the sec login"""
    secPass = input("enter the email password: ")
    data = {"pass": secPass }
    r = s.post(url = "http://localhost/passwords.html", data=data)
    assert ("passwords.html" in r.url)

def get_user_passwords(url):
    r = s.post(url = url)
    print ("user passwords list: ")
    print (r.text)
    assert r.status_code == 200

def get_user_file_list(url):
    r = s.post(url = url)
    print ("user file list: ")
    print (r.text)
    assert r.status_code == 200

def upload_file(url):
    files = {"file": open("text.txt")}
    r = s.post(url = url, files=files)
    assert ("files.html" in r.url)

def logout(url):
    r = s.post(url = url)
    assert ("index.html" in r.url)

if __name__ == "__main__":
    #test_get_request_from_allowed_url("http://localhost")
    #test_get_request_from_not_allowed_url("http://localhost/main.html")
    #test_get_request_from_not_allowed_url("http://localhost/files.html")
    #signup_request("http://localhost/signup", "nimi", "123456", "nimi@walla.com")
    login_request("http://localhost/login", "nimi", "123456")
    #add_password_for_user("http://localhost/addPassword", "facebook", "bar", "123456")
    #test_get_request_from_allowed_url("http://localhost/files.html")
    #get_user_passwords("http://localhost/passwords")
    #get_user_file_list("http://localhost/loadDocs")
    upload_file("http://localhost/uploadFile")
    logout("http://localhost/logout")
    print("Everything passed")

var input = document.getElementById("password");

/*
* get random password and add it to the password area
*/
$.ajax({
    type: "POST",
    url: '/randomPassword',
    success: function(data) {
        input.value = data;
        return data;
    }
});

/*
* add function - show the password in the password area
* it's word because we change the type of the password input from password to text
*/
function showPass()
{
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

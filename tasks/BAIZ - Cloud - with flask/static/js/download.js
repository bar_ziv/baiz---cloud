function downloadFile(name) {
    $.ajax({
        url: "/download",
        data: JSON.stringify({
            "filename": name
        }),
        success: (function(response) {

        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: 'POST'
    });
}

function loadDoc() {
    $.ajax({
        url: "/loadDocs",
        success: (function(response) {
            document.getElementById("files").innerHTML = response;
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        type: 'POST'
    });
}

loadDoc();

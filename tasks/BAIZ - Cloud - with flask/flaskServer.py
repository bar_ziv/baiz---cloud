from flask import Flask, request, send_file, redirect, url_for, session, g, render_template, abort, flash
import modules.upload
import modules.download
import modules.passwords_grt
import modules.db
import modules.mail
import random
import json
import os
from OpenSSL import SSL
from itsdangerous import URLSafeSerializer
from flask_wtf import FlaskForm

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='')
app.secret_key = os.urandom(24)
s = URLSafeSerializer('secret-key')
context = SSL.Context(SSL.TLSv1_2_METHOD)
'''context.use_certificate("cert.crt")
context.use_privatekey("key.key")'''

@app.route('/logout', methods=['POST'])
def logout():
    session.pop('user', None)
    session.pop('allowed', None)
    session.pop('password', None)
    session.pop("passwordAllowed", None)
    return redirect("index.html")

'''
    do login, get username and password and check if they are exist in the db
'''
@app.route('/login', methods=['GET', 'POST'])
def login():
    check = False
    try:
        session.pop('user', None)
        check = modules.db.login(request.form["username"], request.form["password"]) # check login with the db
    except Exception as e: print(e)
    if check:
        session['user'] = request.form['username']
        session["allowed"] = False
        session["password"] = modules.passwords_grt.make_password(6)
        userDetails = modules.db.getUserDetails(session["user"])
        modules.mail.send_mail(userDetails["email"], session["password"])
        return redirect("login.html")
    return redirect("index.html")

'''
    check if the password that the user get in his mail is the same like the password that he enter
'''
@app.route("/login.html", methods=['GET', 'POST'])
def secLogin():
    if (g.user and request.method == "GET"):
        return app.send_static_file("login.html")
    elif (g.user and request.method == "POST"):
        if session["allowed"] == True:
            return redirect("main.html")
        if(request.form["pass"] == session["password"]):
            session["allowed"] = True
            return redirect("main.html")
        else:
            return redirect("login.html")
    return redirect("index.html")

'''
    do signup, get username, password and email, check if the username is already register if don't register him
'''
@app.route('/signup', methods=['POST'])
def addUser():
    try:
        modules.db.addUser(request.form["email"], request.form["username"], request.form["password"]) # register new user
        modules.download.makeDir(request.form["username"])
        return redirect('index.html')
    except Exception as e: print(e)
    return redirect('signup.html')

'''
    return random password for the user
'''
@app.route('/randomPassword', methods=['GET', 'POST'])
def getRandPassword():
    if (not g.user and not g.allowed):
        abort(404)
    return modules.passwords_grt.make_password(random.choice(range(10,40))) #send to the password generator random length

'''
    add new password to the db
'''
@app.route('/addPassword', methods=['POST'])
def addPassword():
    global s
    if (not g.user and not g.allowed):
        abort(404)
    try:
        modules.db.addPassword(g.user, request.form["website"], request.form["username"], s.dumps(request.form["password"])) #add new password
        return redirect('passwords.html')
    except Exception as e: print(e)
    return redirect('addPassword.html')

'''
    send passwords page after rendering
'''
@app.route('/passwords', methods=['POST'])
def passwordPage():
    global s
    if (not g.user and not g.allowed):
        abort(404)
    replace = modules.passwords_grt.passwordContent(g.user, s)
    return json.dumps(replace), 200, {'ContentType':'application/json'} # return success (200)

'''
    send file to user
'''
@app.route('/download/<user>/<file>', methods=['GET', 'POST'])
def download_file(user, file):
    if not (g.user == user) and not g.allowed:
        abort(404)
    filePath = "files//" + user + "//" + file
    try:
        response = s.loads(open(filePath).read())
        return response
    except Exception as e:
        return str(e)

'''
    return list of file names
'''
@app.route('/loadDocs', methods=['POST'])
def fileList():
    if (not g.user and not g.allowed):
        abort(404)
    modules.download.readFiles(g.user)
    path = "files//" + g.user + "//docsFile.txt"
    response = open(path).read()
    if (response == ""):
        response = "there is no files"
    return json.dumps(response), 200, {'ContentType':'application/json'} # return success (200)

'''
    upload the file to the user folder
'''
@app.route('/uploadFile', methods=['POST'])
def upload_file():
    global s
    if (not g.user and not g.allowed):
        abort(404)
    check = modules.upload.uploadFile(request, g.user, s)
    if check:
        return redirect("files.html")
    return redirect("uploadFile.html")

'''
    return files from the static folder
    only allowed files!!
'''
@app.route('/<path:path>', methods=['GET'])
def send_files(path):
    if g.user and g.allowed:
        if(path.endswith(".html")):
            return app.send_static_file(path)
    abort(404)

'''
    return the login page from the tamplates folder
'''
@app.route('/', methods=["GET"])
def file():
    if not g.user and not g.allowed:
        return app.send_static_file("index.html")
    return redirect("main.html")

@app.route('/addPassword.html', methods=["GET"])
def htmlFile1():
    if g.user and g.allowed:
        return app.send_static_file("addPassword.html")
    return redirect("index.html")

@app.route('/settings.html', methods=["GET", "POST"])
def htmlFile2():
    if not g.user and not g.allowed:
        return redirect("index.html")
    userDetails = modules.db.getUserDetails(g.user)
    fileContent = open("static/settings.html").read()
    fileContent = fileContent.replace("replace@email", userDetails["email"])
    fileContent = fileContent.replace("replace@username", userDetails["username"])
    if request.method == 'GET':
        return fileContent
    else:
        if request.form["password"] == "":
            return redirect("settings.html")
        ans = modules.db.changePassword(g.user, request.form)
        content = '''<div class="container-login100-form-btn p-t-10" id="answer">the password has replaced</div>'''
        if not ans:
            content = '''<div class="container-login100-form-btn p-t-10" id="answer">the password hasn't replaced</div>'''
        fileContent = fileContent.replace('''<div class="container-login100-form-btn p-t-10" id="answer"></div''', content)
        return fileContent


@app.route('/files.html', methods=["GET"])
def htmlFile3():
    if g.user and g.allowed:
        return app.send_static_file("files.html")
    return redirect("index.html")

@app.route('/index.html', methods=["GET"])
def htmlFile4():
    if not g.user and not g.allowed:
        return app.send_static_file("index.html")
    return redirect("main.html")

@app.route('/main.html', methods=["GET"])
def htmlFile5():
    if g.user and g.allowed:
        return app.send_static_file("main.html")
    return redirect("index.html")

@app.route('/passwords.html', methods=["GET", "POST"])
def htmlFile6():
    if request.method == "GET":
        if g.user and g.allowed:
            if not ('passwordAllowed' in session):
                session["password"] = modules.passwords_grt.make_password(6)
                userDetails = modules.db.getUserDetails(session["user"])
                modules.mail.send_mail(userDetails["email"], session["password"])
                return app.send_static_file("login.html")
            else:
                return app.send_static_file("passwords.html")
        return redirect("index.html")
    elif request.method == "POST":
        if(request.form["pass"] == session["password"]):
            session["passwordAllowed"] = True
            return redirect("passwords.html")
        else:
            return redirect("main.html")

@app.route('/signup.html', methods=["GET"])
def htmlFile7():
    if not g.user and not g.allowed:
        return app.send_static_file("signup.html")
    return redirect("main.html")

@app.route('/uploadFile.html', methods=["GET"])
def htmlFile8():
    if g.user and g.allowed:
        return app.send_static_file("uploadFile.html")
    return redirect("index.html")

@app.route("/forgot.html", methods=["GET", "POST"])
def htmlFile9():
    if request.method == "GET":
        return app.send_static_file("forgot.html")
    else:
        session["password"] = modules.passwords_grt.make_password(6)
        session["username"] = request.form["username"]
        modules.mail.forgot_password(request.form, session["password"])
        return redirect("password_from_mail.html")

@app.route("/password_from_mail.html", methods=['GET', 'POST'])
def passwordFromMail():
    if (request.method == "GET"):
        return app.send_static_file("password_from_mail.html")
    elif (request.method == "POST"):
        if(request.form["password"] == session["password"]):
            session["allowed"] = True
            return redirect("new_password.html")
        else:
            return redirect("password_from_email.html")
    return redirect("index.html")

@app.route("/new_password.html", methods=['GET', 'POST'])
def newPass():
    if (g.allowed and request.method == "GET"):
        return app.send_static_file("new_password.html")
    elif (g.allowed and request.method == "POST"):
        modules.db.changePassword(session["username"], request.form, False)
        return logout()

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.before_request
def get_user():
    g.user = None
    g.allowed = False
    if ('user' in session):
        g.user = session['user']
    if ('allowed' in session):
        g.allowed = session["allowed"]

if __name__ == "__main__":
    modules.db.init()
    #app.run(port="80", ssl_context=context, threaded=True, debug=True)
    app.run(port="80", threaded=True, debug=True)

import sqlite3
from passlib.hash import sha256_crypt

#we open and close the db in each function because we cant send him as parameter and we cant open him as a global obj

'''
    create new table if it doesn't exist
'''
def init():
    conn = sqlite3.connect('server.db')
    db = conn.cursor()
    db.execute("CREATE TABLE IF NOT EXISTS users(email string, username string primary key, password string)")
    db.execute("CREATE TABLE IF NOT EXISTS passwords(id string, website string, username string, password string)")
    db.close()
    conn.close()

'''
    add new password to the db
'''
def addPassword(username, website, name, password):
    conn = sqlite3.connect('server.db')
    db = conn.cursor()
    # create command that insert new password to the db
    command = "INSERT INTO passwords(id, website, username, password) VALUES('{}', '{}', '{}', '{}')".format(username, website, name, password)
    db.execute(command) # run the command
    conn.commit() # save the changes
    db.close()
    conn.close()

'''
    return the passwords from the db
'''
def getPasswords(name):
    conn = sqlite3.connect('server.db')
    db = conn.cursor()
    command = "SELECT * FROM passwords where id='{}'".format(name)
    db.execute(command)
    data = db.fetchall() # take every password
    db.close()
    conn.close()
    return data

'''
    register new user
'''
def addUser(email, username, password):
    conn = sqlite3.connect('server.db')
    db = conn.cursor()
    # create command that insert new user to the db
    command = "INSERT INTO users(email, username, password) VALUES('{}', '{}', '{}')".format(email, username, sha256_crypt.hash(password))
    db.execute(command) # run the command
    conn.commit() # save the changes
    db.close()
    conn.close()

'''
    check if the username and the password are exist
'''
def login(username, password):
    conn = sqlite3.connect('server.db')
    db = conn.cursor()
    # create command that check if the user is in the db
    command = "select * from users where username='{}';".format(username)
    db.execute(command) # run the command
    ans = db.fetchone() # save the answer
    db.close()
    conn.close()
    if(ans != None and len(ans) > 0): # is the answer is not NONE the user is exist
        if sha256_crypt.verify(password, ans[2]):
            return True
        return False
    return False

"""
    return the user details (email, username, password)
    username = None -> It means that we will search by the email
    email = None -> It means that we will search by the name
"""
def getUserDetails(username=None, email=None):
    conn = sqlite3.connect('server.db')
    db = conn.cursor()
    # create command that check if the user is in the db
    if email == None:
        command = "select * from users where username='{}';".format(username)
    elif username==None:
        command = "select * from users where email='{}';".format(email)
    else:
        return {"email": None, "username": None, "password": None}
    db.execute(command) # run the command
    ans = db.fetchone() # save the answer
    db.close()
    conn.close()
    dict = {"email": ans[0], "username": ans[1], "password": ans[2]}
    return dict

"""
    change user password
    checkPassword = True -> means that in default we will check the password before replace her, unless the user forgot his password   
"""
def changePassword(username, form, checkPassword=True):
    conn = sqlite3.connect('server.db')
    db = conn.cursor()
    # create command that check if the user is in the db
    command = "select password from users where username='{}';".format(username)
    db.execute(command) # run the command
    oldPassword = db.fetchone() # save the answer
    if checkPassword:
        if not (str(oldPassword[0]) == str(form["oldPassword"])):
            db.close()
            conn.close()
            return False
    command = "UPDATE users SET password = '{}' WHERE username = '{}';".format(form["password"], username)
    db.execute(command) # run the command
    conn.commit() # save the changes
    return True

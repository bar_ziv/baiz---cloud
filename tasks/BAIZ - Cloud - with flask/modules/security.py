from werkzeug.utils import secure_filename
from langdetect import detect

'''
    check if the file name is in hebrew - if it is we flip the places of the strings
    if it's not the file name remains the same
'''
def checkFileName(file):
    fileName = file.filename #take the file name
    if (fileName.find('.')!=-1): # found dot
        if(detect(fileName.split('.')[0]) != 'he'): # file name is not if hebraw
            return secure_filename(fileName)  # secure_filename removes hebrew characters, otherwise works fine
        else:
            return fileName.split('.')[0] + "." + fileName.split('.')[1] # flip the places
    return False

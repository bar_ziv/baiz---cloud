import os

"""
    create directory with the user name
"""
def makeDir(username):
    os.mkdir("files//" + username)
    path = "files//" + username + "//docsFile.txt"
    f= open(path,"w+")
    f.close()

'''
        create file that save list of the file that we have
'''
def readFiles(username):
    path = "files//" + username
    docFile = path + "//docsFile.txt"
    fileOb = open(docFile, "r+")
    fileOb.truncate(0) # need '0' when using r+

    var = os.listdir(path) # all the files that are in the dir
    for fileInDir in var:
        name = fileInDir # name of file
        name = name.replace(" ", "%20") # replace whitespace in %20
        id = name[:-4] # supposed to delete the end of the file
        href = "/download/" + username + "/" + name # set function o click
        if(id != "docsFile"):
            # create the line in the file
            fileOb.write('<a href="' + href + '" class="btn btn-outline-info btn-lg btn-block" id="' + id + '" download><button>' + fileInDir + '</button></a>' + '\n' + '<br>')

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from modules.db import getUserDetails

"""
    send mail to the user with password for the second login
"""
def send_mail(reciver, password):
    fromaddr = "baiz.cloud@gmail.com"
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = reciver
    msg['Subject'] = "BAIZ email"

    content = "your password for login is: " + password
    msg.attach(MIMEText(content, 'plain'))

    print (content)
    mailSetup = smtplib.SMTP('smtp.gmail.com', 587)
    mailSetup.ehlo()
    mailSetup.starttls()
    mailSetup.login("baiz.cloud@gmail.com", "@@1q2w3e@@")
    text = msg.as_string()
    mailSetup.sendmail(fromaddr, reciver, text)
    mailSetup.close()

"""
    send mail to the user with password for the verification to change the password of user
"""
def forgot_password(form, password):
    details = getUserDetails(None, form["email"])
    if not form["username"] == details["username"]:
        return False

    fromaddr = "baiz.cloud@gmail.com"
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = form["email"]
    msg['Subject'] = "BAIZ email"

    content = "your password for login is: " + password
    msg.attach(MIMEText(content, 'plain'))

    print (content)
    mailSetup = smtplib.SMTP('smtp.gmail.com', 587)
    mailSetup.ehlo()
    mailSetup.starttls()
    mailSetup.login("baiz.cloud@gmail.com", "@@1q2w3e@@")
    text = msg.as_string()
    mailSetup.sendmail(fromaddr, form["email"], text)
    mailSetup.close()
    return True

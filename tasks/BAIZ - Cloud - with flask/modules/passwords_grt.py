import random
import modules.db

'''
    create a html content with the data (tha password tuple)
'''
def content(data, s):
    returnVal = "<tr>"
    returnVal = returnVal + "<th>" + str(data[1]) + "</th>\n"
    returnVal = returnVal + "<th>" + str(data[2]) + "</th>\n"
    returnVal = returnVal + "<th>" + str(s.loads(data[3])) + "</th>\n"
    returnVal = returnVal + "</tr>"
    return returnVal

'''
    return the passwords as part of html page
'''
def passwordContent(username, s):
    passwordFromDB = modules.db.getPasswords(username) # get the passwords
    returnVal = ''
    if(passwordFromDB == None): # check if there is passwords
        return "empty"
    if not (isinstance(passwordFromDB, tuple)): # it's tuple we have only one password
        for data in passwordFromDB:
            returnVal = returnVal + content(data, s)
    else:
        returnVal = returnVal + content(passwordFromDB, s)

    return returnVal

"""
    the func made the password
"""
def make_password(length):
    password = ''
    chars = 'abcdefghijklmnopqrstuvwxyz123456789'#the cars that can be in the password.
    for c in range(length):#create the password.
        password += random.choice(chars)
    return password


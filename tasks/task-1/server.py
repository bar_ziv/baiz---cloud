from http.server import HTTPServer, BaseHTTPRequestHandler
import os
import traceback

class Serv(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/":
            self.path = "/main.html"
        try:
            file_to_open = open(self.path[1:], "rb").read()
            self.send_response(200)
        except:
            traceback.print_exc()
            file_to_open = "no"
            self.send_response(404)
        try:
            self.end_headers()
            self.wfile.write(file_to_open)
        except:
            traceback.print_exc()

httpd = HTTPServer(('localhost', 80), Serv)
httpd.serve_forever()

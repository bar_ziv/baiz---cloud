from flask import Flask, request, send_from_directory, render_template, redirect, flash, url_for
from werkzeug.utils import secure_filename
from langdetect import detect

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='')

'''
    check if the file name is in hebrew - if it is we flip the places of the strings
    if it's not the file name remains the same
'''
def checkFileName(file):
    fileName = file.filename #take the file name
    if (fileName.find('.')!=-1): # found dot
        if(detect(fileName.split('.')[0]) != 'he'): # file name is not if hebraw
            return secure_filename(fileName)  # secure_filename removes hebrew characters, otherwise works fine
        else:
            return fileName.split('.')[0] + "." + fileName.split('.')[1] # flip the places
    return False

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file:
            filename = checkFileName(file)
            if(filename != False): # allowed file name
                file.save("files//" + filename) # save to some folder
                return 'file uploaded'
            return 'file is not good'

'''
    return files from the static folder
    only allowed files!!
'''
@app.route('/<path:path>')
def send_files(path):
    return app.send_static_file(path)

'''
    return the login page from the tamplates folder
'''
@app.route('/')
def file():
    return render_template("upload.html")

if __name__ == "__main__":
    app.run(debug=True, port="80")

from flask import Flask, request
import modules.upload
import modules.download

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='')

'''
    upload the file to the user folder
'''
@app.route('/loadDocs', methods=['GET', 'POST'])
def download_file():
    modules.download.readFiles()
    response = open("files//docsFile.txt").read()
    return response

'''
    upload the file to the user folder
'''
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    modules.upload.uploadFile(request)

'''
    return files from the static folder
    only allowed files!!
'''
@app.route('/<path:path>')
def send_files(path):
    if(path.endswith(".html")):
        return app.send_static_file(path)
    return app.render_template('404.html')

'''
    return the login page from the tamplates folder
'''
@app.route('/')
def file():
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run(debug=True, port="80")

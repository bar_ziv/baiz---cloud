import socket
from scapy.all import *
import requests
import time
import sys
import psutil
import json

ADDR = 'localhost' #This computer
PACKS = 10
ADDRESS = "8.8.8.8"
PORT = 53
URL = "http://freegeoip.net"
FORMAT = "/json/"
MY_IP = IP().src
HTTPS = 443
NAME = "Name - "
CHROME = "chrome.exe"
HTTPS = "443"
UNKNOWN = "Unknown"
CN = "country_name"
SEP = ",,,"
ONE = 1
STATE = "State"
BOOL_T = "Bool"
IP_T = "IP"
PORT_T = "Port"
PROG_T = "Program"
SIZE_T = "Size"
DNS_T = "DNS"
INCOME = "From in"
OUTCOME = "From out"
IS_DNS_T = "In"
IS_DNS_F = "Not in"

def main():
	place = 0      #int
	temp = ""      #str
	
	# Create a TCP/IP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	# Binding to local port 53
	server_address = (ADDR, PORT)
	
	'''name = input("Enter name please: ") #The user receives the name
	send_data = NAME + name #User name entered
	send_data = bytes(send_data, encoding = "ascii") #To make it possible to send, we will turn it into houses
	sock.sendto(send_data,server_address) #Sends the information, port and address'''

	while(ONE):
		all_Data = []  #list
		place = 0 #int
		print("------------------") #To make it beautiful
		print("Start sniffing")     #Print that started snuff
		print("------------------") #To make it beautiful

		packets = sniff(count = PACKS, lfilter = is_udp_tcp) #From the 100-packet branch,and we transfer the name of the function as a variable
		print("------------------") #To make it beautiful
		print("Start Editing packets") #To make it beautiful
		print("------------------") #To make it beautiful
		
		for packet in packets: #Go through the packages and check for a package inside
			new_mil = {}
			if IP in packet: #if the ip in packet 
				new_mil[IP_T] =  find_ip(packet) #Send it a function find_ip
				
				#all_Data.append({IP_T : ip}) #Sends to  code boss
				
				new_mil[STATE] = find_state(new_mil[IP_T]) #Goes to a function that found the state

				#all_Data[place][STATE] = state #Put into the
				
				in_out = is_in_out(packet) #Goes to a function that will return my ip
				if(in_out):#If my ip
					#all_Data[place][BOOL_T] = INCOME  #Insert
					new_mil[BOOL_T] = INCOME
				else:
					#all_Data[place][BOOL_T] = OUTCOME #It's not the source, it's the target
					new_mil[BOOL_T] = OUTCOME
				#print(all_Data[place][IP_T]) #Reads and prints all functions
				new_mil[PORT_T] = out_port(packet)				
				new_mil[PROG_T] = find_program(new_mil[IP_T] , new_mil[PORT_T])
				new_mil[SIZE_T] = len(packet)
				new_mil[DNS_T] = is_dns(packet)
				all_Data.append(new_mil)
				
				place += ONE
				
#		for data in all_Data:
#			temp += str(data)
					
		#temp = name + SEP+ temp
		#send_packs = temp.encode("utf-8")
		send_packs = json.dumps(all_Data)
		send_packs = send_packs.encode()
		sock.sendto(send_packs,server_address)
		temp = ""
				
	
	sock.close()
	
"""This function check if the packet that recieved is a TCP or UDP"""
def is_udp_tcp(packet):
	return (UDP in packet or TCP in packet) #Returns the protocol

"""This Function check the IP of the computer we are talking with """
def find_ip(packet):
	if str(packet[IP].src) == MY_IP: #If in the ip source package is my ip
		return packet[IP].dst #Return the dst ip
	else: #if not
		return packet[IP].src #Return the source ip

"""This function find the state that the packet came from by the IP of the sender/reciever """
def find_state(ip):
	try:
		postdata =  URL + FORMAT + ip #Takes address and  ip and /json
		answer = requests.get(postdata) #We will try to get the answer from the data of postdata
		data = answer.json()
		return data[CN] #Returns the country 
	except:
		pass #to disregard

"""check if the packet is from us or from the outside"""
def is_in_out(packet):
	return MY_IP == packet[IP].src #Returns if my IP is the source

"""This function return The port of the server we talk with"""	
def out_port(packet):
	if str(packet[IP].src) == MY_IP: #if my IP is the source
		if TCP in packet:  #Checks if tcp in package
			port = packet[TCP].dport #You will save the destination port in the port
		else: #Checks if udp in package
			port = packet[UDP].dport #You will save the destination port in the port
	else:
		if TCP in packet: #Checks if tcp in package
			port = packet[TCP].sport #You will save the source port in the variable
		else: #Checks if udp in package
			port = packet[UDP].sport #You will save the source port in the variable

	return port#Return the port you found

"""This function find the program that the packet came from"""
def find_program(ip,port):
	programs = {} #dict
	
	for program in psutil.process_iter():#This for make a dictionary of all the programs by ports
		try: #try out
			programs[program.pid] = program.name()
		except psutil.Error:
			pass
			
	for ports in psutil.net_connections(kind='inet'):#match the Ip of the packet to the Port in the dictionary
		try:
			if ip == (ports.raddr[0]): 
				return(programs.get(ports.pid))
		except:
			pass
				
	if str(port) == HTTPS:
		return(CHROME) 
		
	return(UNKNOWN) #For packages whose software is not recognized
	
"""This function check if the packet is a DNS packet"""
def is_dns(packet):
	if DNS in packet: #Checks if package in dns
		return (IS_DNS_T) #If successful, then it's inside
	return(IS_DNS_F) #If not successful, then it's inside
main()

org 100

jmp main

lock1:
    mov ax, 12h
    cmp [23], ax
    jne lock2

lock2: 
    push cx
    mov cx, 10 
    mov bx, 0
    someLoop:
        add bx, cx
        loop someLoop
    pop cx
    jmp return
    
main:
mov cx,1010b
mainLoop: 
mov ax, 17
cmp [22], ax
je lock1
jmp lock2
loop mainLoop 

return:
cmp [2], bx
jne main
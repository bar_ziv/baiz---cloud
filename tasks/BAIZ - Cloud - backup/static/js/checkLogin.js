if(document.cookie == "")
{
    document.cookie = "isLogged=False;";
}

var url = window.location.href;
if(url.includes("index.html") || url == "http://localhost/"){
    var c = document.cookie.split("=");
    if (c[1] == "True"){
        //redirect to page
        window.location.href = "main.html";
    }
}
else{
    var c = document.cookie.split("=");
    if (c[1] == "False"){
        //redirect to page
        window.location.href = "index.html";
    }
}

function logout()
{
    document.cookie = "isLogged=False;";
}

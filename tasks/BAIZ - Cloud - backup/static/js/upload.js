function submitForm(e) {
    e.preventDefault();

    var reader = new FileReader();
    var data = new FormData(); // create form data object
    var input = document.querySelector('input[type=file]');

    reader.onload = function(){
        var dataURL = reader.result;
        var output = document.getElementById('output');
        output.src = dataURL;
    }

    reader.readAsDataURL(input.files[0]);
    data.append("file", input.files[0]); // add file to form data

    // sync. request to server
    $.ajax({
        url: '/upload',
        data: data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function(data) {
            console.log(data);
        }
    });

    window.location.href = "files.html";
}

document.querySelector('form').addEventListener('submit', submitForm);

import modules.security
from flask import request, flash, redirect

def uploadFile(req):
    if req.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file:
            filename = modules.security.checkFileName(file)
            if(filename != False): # allowed file name
                file.save("files//" + filename) # save to some folder
                return 'file uploaded'
            return 'file is not good'

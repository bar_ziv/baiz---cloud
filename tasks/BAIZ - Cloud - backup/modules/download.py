import os

def readFiles():
    path = "files"
    fileOb = open("files/docsFile.txt", "r+")
    fileOb.truncate(0) # need '0' when using r+

    var = os.listdir(path)
    for fileInDir in var:
        name = fileInDir
        name = name.replace(" ", "%20")
        id = name[:-4]
        href = "downloadFile('" + id + "')"
        if(id != "docsFile"):
            fileOb.write('<a onclick="' + href + '" class="btn btn-outline-info btn-lg btn-block" id="' + id + '"><button>' + fileInDir + '</button></a>' + '\n' + '<br>')
